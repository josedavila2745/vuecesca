Vue.component('fd-producto', {
    props: ['num', 'prod', 'pvp', 'hay', 'imag'],
    data: function() { return { cant: 0, subtotal: 0, ver: false } },
    template: `<div>
              <h1>{{num}}: {{prod}} S/ {{pvp}} </h1>
              <br>
              <img :src="imag"> 
              <br>
              Comprar: <input type='number' v-model="cant" min= 0>  &nbsp;&nbsp; {{subtotal}}
              <br>
              <button class="btn btn-success" @click="agregar( [cant, prod, pvp, hay])">Agregar</button> &nbsp;&nbsp;   Total: {{ ttotal }}
              <br> <button class="btn btn-danger" @click="cambiar">Carrito</button>
              <div v-if="ver">
              <h3> Si si si</h3>
              </div>
              </div>`,
    watch: {
        cant: function() {
            this.subtotal = this.cant * this.pvp;
        }
    },
    computed: {
        ...Vuex.mapState(['ttotal', 'carrito'])
    },
    methods: {
        ...Vuex.mapMutations(['agregar', 'quitar']),
        cambiar() {
            this.ver = (this.ver) ? false : true;
            console.log(this.ver);
        }
    },

});

const store = new Vuex.Store({
    state: { ttotal: 0, carrito: [] },
    mutations: {
        agregar(state, dd) {
            console.log(dd)
            state.ttotal += parseFloat(dd[0]) * parseFloat(dd[2])
            state.carrito.push({ 'unds': dd[0], 'prod': dd[1], 'st': parseFloat(dd[0]) * parseFloat(dd[2]) })
            console.log(state.carrito)
        }
    },

})

var app = new Vue({
    el: '#app',
    store,
    data: {
        total: 0,
        stock: [
            { producto: 'Bicarbonato de Sodio', precio: 15, disponibilidad: 0, cantidad: 0, img: "../img/bicarbonato.jpeg" },
            { producto: 'Cloruro de Magnesio', precio: 15, disponibilidad: 1, cantidad: 0, img: "../img/cloruro.jpeg" },
            { producto: 'Colágeno', precio: 30, disponibilidad: 0, cantidad: 10, img: "../img/colageno.jpeg" },
            { producto: 'Multivitamínico', precio: 80, disponibilidad: 1, cantidad: 0, img: "../img/multivitaminico.jpeg" },
            { producto: 'Aceite de Magnesio', precio: 18, disponibilidad: 0, cantidad: 0, img: "../img/aceite.jpeg" },
            { producto: 'Aromaterapia', precio: 15, disponibilidad: 1, cantidad: 0, img: "../img/aroma.jpeg" },
            { producto: 'Maca Orgánica', precio: 30, disponibilidad: 0, cantidad: 0, img: "../img/maca.jpeg" },
            { producto: 'Carbonato de Mágnesio', precio: 30, disponibilidad: 10, cantidad: 0, img: "../img/carbonato.jpeg" },
            { producto: 'Celulas Madres', precio: 80, disponibilidad: 1, cantidad: 0, img: "../img/celulas.jpeg" },
            { producto: 'Aloe', precio: 40, disponibilidad: 0, cantidad: 0, img: "../img/aloe.jpeg" },
            { producto: 'Nopal', precio: 30, disponibilidad: 1, cantidad: 0, img: "../img/nopal.jpeg" },
            { producto: 'Perfumes', precio: 18, disponibilidad: 0, cantidad: 0, img: "../img/perfumes.jpeg" }
        ],


    }
});