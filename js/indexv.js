const store = new Vuex.Store({
    state: {
        myTotal: 0
    },
    getters: {
        someFilter: state => {
            return state.myTotal[0]
        }
    },
    mutations: {
        addToArray(state, value) {
            state.myTotal.push(value)
        }
    },
    actions: {
        addToArray(context) {
            context.commit('addToArray');
        }
    }
});

Vue.component('mi-producto', {
    props: ['num', 'prod', 'precio', 'disponibilidad', 'imag'],
    data: function() { return { cant: 0, subtotal: 0 } },
    template: `<div>
              <h1>{{num}}: {{prod}} S/ {{precio}} </h1>
              <br>
              <img :src="imag"> 
              <br>
              Comprar: <input type='number' v-model="cant" min= 0>  &nbsp;&nbsp; {{subtotal}}
              <br>
              Total: {{ myTotal }}
              </div>`,
    watch: {
        cant: function() {
            //if (this.cant <= this.disponibilidad) { this.subtotal = this.cant * this.precio; }
            this.subtotal = this.cant * this.precio;
        }
    }

});

var app = new Vue({
    el: '#app',
    store,
    data: {
        cant: 0,
        valor: 12,
        total: 0,
        stock: [
            { producto: 'Bicarbonato de Sodio', precio: 15, disponibilidad: 10, cantidad: 0, img: "../img/bicarbonato.jpeg" },
            { producto: 'Cloruro de Magnesio', precio: 15, disponibilidad: 10, cantidad: 0, img: "../img/cloruro.jpeg" },
            { producto: 'Colágeno', precio: 30, disponibilidad: 10, cantidad: 10, img: "../img/colageno.jpeg" },
            { producto: 'Multivitamínico', precio: 80, disponibilidad: 10, cantidad: 0, img: "../img/multivitaminico.jpeg" },
            { producto: 'Aceite de Magnesio', precio: 18, disponibilidad: 10, cantidad: 0, img: "../img/aceite.jpeg" },
            { producto: 'Aromaterapia', precio: 15, disponibilidad: 10, cantidad: 0, img: "../img/aroma.jpeg" },
            { producto: 'Maca Orgánica', precio: 30, disponibilidad: 10, cantidad: 0, img: "../img/maca.jpeg" },
            { producto: 'Carbonato de Mágnesio', precio: 30, disponibilidad: 10, cantidad: 0, img: "../img/carbonato.jpeg" },
            { producto: 'Celulas Madres', precio: 80, disponibilidad: 10, cantidad: 0, img: "../img/celulas.jpeg" },
            { producto: 'Aloe', precio: 40, disponibilidad: 10, cantidad: 0, img: "../img/aloe.jpeg" },
            { producto: 'Nopal', precio: 30, disponibilidad: 10, cantidad: 0, img: "../img/nopal.jpeg" },
            { producto: 'Perfumes', precio: 18, disponibilidad: 10, cantidad: 0, img: "../img/perfumes.jpeg" }
        ],


    },
    watch: {
        cant: function() {
            this.total = this.cant * this.valor;
        }
    },
    computed: {
        total1: function() { return this.cant * this.valor; }
    }
});